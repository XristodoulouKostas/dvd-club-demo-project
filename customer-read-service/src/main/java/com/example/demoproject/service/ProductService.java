package com.example.demoproject.service;

import com.example.demoproject.entity.Customer;
import com.example.demoproject.entity.Product;
import com.example.demoproject.repository.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ProductService {

    private CustomerRepository customerRepository;

    public ProductService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Product> getCustomerProducts(String customerId) {
        Assert.notNull(customerId, "Customer Id cannot be null");
        List<Product> products = customerRepository.getCustomer(customerId).getProducts();
        if (Objects.isNull(products)) {
            products = new ArrayList<>();
        }
        return products;
    }

    public void addProductToCustomer(String customerId, Product product) {
        Assert.notNull(customerId, "Customer Id cannot be null");
        Assert.notNull(product, "Product cannot be null");
        Customer customer = customerRepository.getCustomer(customerId);
        customer.addProduct(product);
    }
}
