package com.example.demoproject.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Customer {

    private String id;
    private String firstName;
    private String lastName;
    private String afm;
    private String phoneNumber;
    private String email;
    private List<Product> products;

    public void addProduct(Product product) {
        if (Objects.isNull(this.products)) {
            this.products = new ArrayList<>();
        }
        this.products.add(product);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}