package com.example.demoproject.entity;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class Product {

    private String productId;
    private String productSapCode;
    private String productDescription;
    private int productQuantity;
    private String loanStartDate;
    private String loanEndDate;
    private BigDecimal nominalPrice;
    private BigDecimal penaltyPricePerDay;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductSapCode() {
        return productSapCode;
    }

    public void setProductSapCode(String productSapCode) {
        this.productSapCode = productSapCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getNominalPrice() {
        return nominalPrice;
    }

    public void setNominalPrice(BigDecimal nominalPrice) {
        this.nominalPrice = nominalPrice;
    }

    public BigDecimal getPenaltyPricePerDay() {
        return penaltyPricePerDay;
    }

    public void setPenaltyPricePerDay(BigDecimal penaltyPricePerDay) {
        this.penaltyPricePerDay = penaltyPricePerDay;
    }
}
