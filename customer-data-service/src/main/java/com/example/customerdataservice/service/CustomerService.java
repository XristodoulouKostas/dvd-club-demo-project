package com.example.customerdataservice.service;

import com.example.customerdataservice.dto.CustomerDto;
import com.example.customerdataservice.dto.ProductDto;
import com.example.customerdataservice.entity.Customer;
import com.example.customerdataservice.entity.Product;
import com.example.customerdataservice.exception.DataNotFoundException;
import com.example.customerdataservice.exception.MessageProduceException;
import com.example.customerdataservice.repository.CustomerRepository;
import com.example.customerdataservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private MessageProducerService messageProducerService;

    public CustomerDto getCustomer(long id) throws DataNotFoundException {
        return convertCustomerEntityToDto(customerRepository.findById(id).orElseThrow(DataNotFoundException::new));
    }

    public List<CustomerDto> getCustomers() {
        List<Customer> customers = customerRepository.findAll();
        if (Objects.isNull(customers)) {
            customers = new ArrayList<>();
        }
        return customers.stream().map(this::convertCustomerEntityToDto).collect(Collectors.toList());
    }

    public long createNewCustomer(CustomerDto customer) throws MessageProduceException {
        if (customer.getId() != 0) {
            throw new IllegalArgumentException("Customer Id must be null");
        }
        Customer newCustomer = customerRepository.save(convertCustomerDtoToEntity(customer));
        // messageProducerService.produceMessage(KafkaEventAction.CREATE, List.of(convertCustomerEntityToDto(newCustomer)), null);
        return newCustomer.getId();
    }

    public void updateCustomer(CustomerDto customer) {
        Assert.isTrue(customer.getId() > 0, "Customer Id cannot be negative or zero");
        customerRepository.save(convertCustomerDtoToEntity(customer));
    }

    public void addProductToCustomer(long customerId, ProductDto productDto) throws DataNotFoundException {
        Assert.isTrue(customerId > 0, "Customer Id cannot be negative or zero");
        Customer customer = customerRepository.findById(customerId).orElseThrow(DataNotFoundException::new);
        customer.addProduct(new Product(productDto, customer));
        customerRepository.save(customer);
    }

    public void updateproductOfCustomer(long customerId, ProductDto productDto) throws DataNotFoundException {
        Assert.isTrue((customerId == productDto.getCustomerId()), "Customer id not maching");
        Customer customer = customerRepository.findById(customerId).orElseThrow(DataNotFoundException::new);
        productRepository.save(new Product(productDto, customer));
    }

    private Customer convertCustomerDtoToEntity(CustomerDto dto) {
        Customer entity = new Customer(dto);
        if (!CollectionUtils.isEmpty(dto.getProducts())) {
            List<Product> products = new ArrayList<>();
            for (ProductDto productDto : dto.getProducts()) {
                products.add(new Product(productDto, entity));
            }
            entity.setProducts(products);
        }
        return entity;
    }

    private CustomerDto convertCustomerEntityToDto(Customer entity) {
        return new CustomerDto(entity);
    }
}
