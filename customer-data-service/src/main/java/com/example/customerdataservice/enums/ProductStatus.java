package com.example.customerdataservice.enums;

public enum ProductStatus {

    ON_LOAN("On Loan"),
    RETURNED("Returned");

    private String label;

    ProductStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
