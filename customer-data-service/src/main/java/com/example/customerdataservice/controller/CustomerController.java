package com.example.customerdataservice.controller;

import com.example.customerdataservice.dto.CustomerDto;
import com.example.customerdataservice.dto.ProductDto;
import com.example.customerdataservice.exception.DataNotFoundException;
import com.example.customerdataservice.exception.MessageProduceException;
import com.example.customerdataservice.service.CustomerService;
import com.example.customerdataservice.service.MessageProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private MessageProducerService messageProducerService;

    @GetMapping
    public ResponseEntity<List<CustomerDto>> getCustomers() {
        return ResponseEntity.ok(customerService.getCustomers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> getCustomer(@PathVariable long id) throws DataNotFoundException {
        return ResponseEntity.ok(customerService.getCustomer(id));
    }

    @PostMapping
    public ResponseEntity<Long> createCustomer(@RequestBody CustomerDto customer) throws MessageProduceException {
        return ResponseEntity.ok(customerService.createNewCustomer(customer));
    }

    @PutMapping("/{id}")
    public ResponseEntity updateCustomer(@RequestBody CustomerDto customer, @PathVariable long id) {
        customer.setId(id);
        customerService.updateCustomer(customer);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/{id}/product")
    public ResponseEntity addProductToCustomer(@RequestBody ProductDto productDto, @PathVariable long id)
        throws DataNotFoundException {
        customerService.addProductToCustomer(id, productDto);
        return ResponseEntity.accepted().build();
    }

    @PutMapping("/{id}/product")
    public ResponseEntity updateProduct(@RequestBody ProductDto productDto, @PathVariable long id)
        throws DataNotFoundException {
        customerService.addProductToCustomer(id, productDto);
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity handleDataNotFoundException(DataNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(MessageProduceException.class)
    public ResponseEntity handleMessageProduceException(MessageProduceException ex) throws JsonProcessingException {
        messageProducerService.invalidateReadDatabase();
        return ResponseEntity.ok().build();
    }

}
