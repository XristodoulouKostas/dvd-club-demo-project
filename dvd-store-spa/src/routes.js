import Home from './components/Home.vue';
import CustomerView from './components/customer/CustomerView.vue';
import CustomerForm from './components/customer/CustomerForm.vue';

export const routes = [
    { path: '', component: Home },
    { path: '/customer', component: CustomerView },
    { path: '/customer/register', component: CustomerForm }
];