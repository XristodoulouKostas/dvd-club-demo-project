package com.example.customerdataservice.enums;

public enum KafkaEventAction {

    CREATE,
    UPDATE,
    DELETE,
    INVALIDATE

}
