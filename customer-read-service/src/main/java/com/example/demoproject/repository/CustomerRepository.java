package com.example.demoproject.repository;

import com.example.demoproject.entity.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class CustomerRepository {

    private MongoTemplate mongoTemplate;
    private String customerCollection;

    public CustomerRepository(MongoTemplate mongoTemplate, @Value("${mongo.collection.customers}") String collection) {
        this.mongoTemplate = mongoTemplate;
        this.customerCollection = collection;
    }

    public List<Customer> getCustomers() {
        return mongoTemplate.findAll(Customer.class, customerCollection);
    }

    public List<Customer> getCustomers(Customer customer) {
        Query query = new Query();
        query.addCriteria(Criteria.byExample(customer));
        return mongoTemplate.find(query, Customer.class, customerCollection);
    }

    public Customer getCustomer(String customerId) {
        return mongoTemplate.findById(customerId, Customer.class, customerCollection);
    }

    public Customer saveCustomer(Customer customer) {
        return mongoTemplate.save(customer, customerCollection);
    }

    public boolean customerExists(String customerId) {
        return Objects.nonNull(getCustomer(customerId));
    }

}
