import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import { routes } from './routes';

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({routes: routes, mode: 'history'});

Vue.http.options.root = 'http://localhost:8085';
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  next();
});

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
