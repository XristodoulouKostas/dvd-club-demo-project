package com.example.customerdataservice.entity;

import com.example.customerdataservice.dto.ProductDto;
import com.example.customerdataservice.enums.ProductStatus;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
@EntityListeners(AuditingEntityListener.class)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long productId;

    @ManyToOne
    @JoinColumn(name="CUSTOMER_ID", nullable=false)
    private Customer customer;

    @Column(name = "PRODUCT_SAP_CODE")
    private String productSapCode;
    @Column(name = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Column(name = "PRODUCT_QUANTITY")
    private int productQuantity;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "LOAN_START_DATE")
    private String loanStartDate;
    @Column(name = "LOAN_END_DATE")
    private String loanEndDate;
    @Column(name = "NOMINAL_PRICE")
    private BigDecimal nominalPrice;
    @Column(name = "PENALTY_PRICE_PER_DAY")
    private BigDecimal penaltyPricePerDay;
    @CreatedDate
    @Column(name = "CREATED_AT")
    private LocalDateTime createdAt;
    @LastModifiedDate
    @Column(name = "UPDATED_AT")
    private LocalDateTime updatedAt;

    public Product() {
    }

    public Product(ProductDto productDto, Customer customer) {
        this.productId = productDto.getProductId();
        this.customer = customer;
        this.productSapCode = productDto.getProductSapCode();
        this.productDescription = productDto.getProductDescription();
        this.productQuantity = productDto.getProductQuantity();
        this.loanStartDate = productDto.getLoanStartDate();
        this.loanEndDate = productDto.getLoanEndDate();
        this.nominalPrice = productDto.getNominalPrice();
        this.penaltyPricePerDay = productDto.getPenaltyPricePerDay();
        this.status = productDto.getStatus().name();
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getProductSapCode() {
        return productSapCode;
    }

    public void setProductSapCode(String productSapCode) {
        this.productSapCode = productSapCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getNominalPrice() {
        return nominalPrice;
    }

    public void setNominalPrice(BigDecimal nominalPrice) {
        this.nominalPrice = nominalPrice;
    }

    public BigDecimal getPenaltyPricePerDay() {
        return penaltyPricePerDay;
    }

    public void setPenaltyPricePerDay(BigDecimal penaltyPricePerDay) {
        this.penaltyPricePerDay = penaltyPricePerDay;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status.getLabel();
    }
}
