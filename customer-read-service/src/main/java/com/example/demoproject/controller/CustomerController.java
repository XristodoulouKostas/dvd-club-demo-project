package com.example.demoproject.controller;

import com.example.demoproject.entity.Customer;
import com.example.demoproject.entity.Product;
import com.example.demoproject.exception.DataNotFoundException;
import com.example.demoproject.service.CustomerService;
import com.example.demoproject.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class CustomerController {

    private CustomerService customerService;
    private ProductService productService;

    public CustomerController(CustomerService customerService, ProductService productService) {
        this.customerService = customerService;
        this.productService = productService;
    }

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> getCustomers(@RequestParam Optional<String> id,
        @RequestParam Optional<String> afm, @RequestParam Optional<String> phone,
        @RequestParam Optional<String> lastName) throws DataNotFoundException {
        List<Customer> customers;
        if (id.isPresent()) {
            customers = List.of(customerService.getCustomer(id.get()));
        } else {
            customers = customerService.getCustomers(afm, phone, lastName);
        }
        return ResponseEntity.ok(customers);
    }

    @PostMapping("/customer")
    public ResponseEntity<String> saveNewCustomer(@RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.saveCustomer(customer));
    }

    @PutMapping("/customer")
    public ResponseEntity updateCustomer(@RequestBody Customer customer) throws DataNotFoundException {
        customerService.updateCustomer(customer);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/customer/{customerId}/product")
    public ResponseEntity<List<Product>> getCustomerProducts(@PathVariable String customerId) {
        return ResponseEntity.ok(productService.getCustomerProducts(customerId));
    }

    @PostMapping("/customer/{customerId}/product")
    public ResponseEntity addCustomerProduct(@PathVariable String customerId,
        @RequestBody Product product) {
        productService.addProductToCustomer(customerId, product);
        return ResponseEntity.ok().build();
    }
}
