package com.example.demoproject.service;

import com.example.demoproject.entity.Customer;
import com.example.demoproject.entity.Product;
import com.example.demoproject.exception.DataNotFoundException;
import com.example.demoproject.repository.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomers() {
        List<Customer> customers = customerRepository.getCustomers();
        if (Objects.isNull(customers)) {
            customers = new ArrayList<>();
        }
        return customers;
    }

    public List<Customer> getCustomers(Optional<String> afm, Optional<String> phone, Optional<String> lastName) {
        List<Customer> customers;
        Customer searchCriteria = new Customer();
        afm.ifPresent(x -> searchCriteria.setAfm(x));
        phone.ifPresent(x -> searchCriteria.setPhoneNumber(x));
        lastName.ifPresent(x -> searchCriteria.setLastName(x));
        customers = customerRepository.getCustomers(searchCriteria);
        if (Objects.isNull(customers)) {
            customers = new ArrayList<>();
        }
        return customers;
    }

    public Customer getCustomer(String customerId) throws DataNotFoundException {
        Customer customer = customerRepository.getCustomer(customerId);
        if (Objects.isNull(customer)) {
            throw new DataNotFoundException(MessageFormat.format("Customer with id {0} does not exist.", customerId));
        }
        return customer;
    }

    public String saveCustomer(Customer customer) {
        customer.setId(null);
        return customerRepository.saveCustomer(customer).getId();
    }

    public void updateCustomer(Customer customer) throws DataNotFoundException {
        Assert.notNull(customer.getId(), "Customer id cannot be null");
        if (!customerRepository.customerExists(customer.getId())) {
            throw new DataNotFoundException("Customer does not exist.");
        }
        customerRepository.saveCustomer(customer);
    }



    private Customer getSampleCustomer() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now();

        Product product = new Product();
        product.setProductId("1");
        product.setProductSapCode("123456789");
        product.setProductQuantity(1);
        product.setProductDescription("Pro Evolution Soccer 2019");
        product.setLoanStartDate(zonedDateTime.toString());
        product.setLoanEndDate(zonedDateTime.plusDays(3).toString());
        product.setNominalPrice(BigDecimal.TEN);
        product.setPenaltyPricePerDay(BigDecimal.valueOf(1));

        Customer customer = new Customer();
        customer.setFirstName("Nikos");
        customer.setLastName("Matsamplokos");
        customer.setAfm("123321123");
        customer.setEmail("fousekis.trigwniko.radar@hotmail.gr");
        customer.setPhoneNumber("698123456A");
        customer.setProducts(List.of(product));

        return customer;
    }

}
