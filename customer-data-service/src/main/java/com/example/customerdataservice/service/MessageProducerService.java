package com.example.customerdataservice.service;

import com.example.customerdataservice.dto.CustomerDto;
import com.example.customerdataservice.dto.KafkaEvent;
import com.example.customerdataservice.enums.KafkaEventAction;
import com.example.customerdataservice.exception.MessageProduceException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageProducerService {

    private KafkaTemplate<String, String> kafkaTemplate;
    private String kafkaTopic;

    public MessageProducerService(KafkaTemplate<String, String> kafkaTemplate, @Value("kafka.produce-topic") String kafkaTopic) {
        this.kafkaTemplate = kafkaTemplate;
        this.kafkaTopic = kafkaTopic;
    }

    public void produceMessage(KafkaEventAction action, List<CustomerDto> customers, List<String> ids)
            throws MessageProduceException {
        KafkaEvent event = KafkaEvent.newBuilder()
                .setAction(action)
                .setCustomers(customers)
                .setIds(ids)
                .createKafkaEvent();

        ObjectMapper om = new ObjectMapper();

        try {
            kafkaTemplate.send(kafkaTopic, om.writeValueAsString(event));
        } catch (Exception ex) {
            throw new MessageProduceException("Could not send Message to Kafka");
        }
    }

    public void invalidateReadDatabase() throws JsonProcessingException {
        KafkaEvent event = KafkaEvent.newBuilder()
                .setAction(KafkaEventAction.INVALIDATE)
                .createKafkaEvent();

        ObjectMapper om = new ObjectMapper();

        kafkaTemplate.send(kafkaTopic, om.writeValueAsString(event));
    }

}
