package com.example.customerdataservice.exception;

public class MessageProduceException extends Exception {

    public MessageProduceException() {
    }

    public MessageProduceException(String message) {
        super(message);
    }
}
