package com.example.customerdataservice.dto;

import com.example.customerdataservice.entity.Product;
import com.example.customerdataservice.enums.ProductStatus;

import java.math.BigDecimal;

public class ProductDto {

    private long productId;
    private long customerId;
    private String productSapCode;
    private String productDescription;
    private int productQuantity;
    private ProductStatus status;
    private String loanStartDate;
    private String loanEndDate;
    private BigDecimal nominalPrice;
    private BigDecimal penaltyPricePerDay;

    public ProductDto() {
    }

    public ProductDto(Product productEntity) {
        this.productId = productEntity.getProductId();
        this.customerId = productEntity.getCustomer().getId();
        this.productSapCode = productEntity.getProductSapCode();
        this.productDescription = productEntity.getProductDescription();
        this.productQuantity = productEntity.getProductQuantity();
        this.loanStartDate = productEntity.getLoanStartDate();
        this.loanEndDate = productEntity.getLoanEndDate();
        this.nominalPrice = productEntity.getNominalPrice();
        this.penaltyPricePerDay = productEntity.getPenaltyPricePerDay();
        this.status = ProductStatus.valueOf(productEntity.getStatus());
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductSapCode() {
        return productSapCode;
    }

    public void setProductSapCode(String productSapCode) {
        this.productSapCode = productSapCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getNominalPrice() {
        return nominalPrice;
    }

    public void setNominalPrice(BigDecimal nominalPrice) {
        this.nominalPrice = nominalPrice;
    }

    public BigDecimal getPenaltyPricePerDay() {
        return penaltyPricePerDay;
    }

    public void setPenaltyPricePerDay(BigDecimal penaltyPricePerDay) {
        this.penaltyPricePerDay = penaltyPricePerDay;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }
}
