package com.example.customerdataservice.dto;

import com.example.customerdataservice.enums.KafkaEventAction;

import java.util.List;

public class KafkaEvent {

   private final String action;
   private final List<CustomerDto> customers;
   private final List<String> ids;

    private KafkaEvent(Builder builder) {
        this.action = builder.action;
        this.customers = builder.customers;
        this.ids = builder.ids;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {

        private String action;
        private List<CustomerDto> customers;
        private List<String> ids;

        private Builder() {
        }

        public Builder setAction(KafkaEventAction action) {
            this.action = action.name();
            return this;
        }

        public Builder setCustomers(List<CustomerDto> customers) {
            this.customers = customers;
            return this;
        }

        public Builder setIds(List<String> ids) {
            this.ids = ids;
            return this;
        }

        public KafkaEvent createKafkaEvent() {
            return new KafkaEvent(this);
        }
    }
}
