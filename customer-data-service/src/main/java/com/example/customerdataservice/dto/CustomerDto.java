package com.example.customerdataservice.dto;

import com.example.customerdataservice.entity.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CustomerDto {

    private long id;

    private String firstName;
    private String lastName;
    private String afm;
    private String phoneNumber;
    private String email;
    private List<ProductDto> products;

    public CustomerDto() {
    }

    public CustomerDto(Customer customerEntity) {
        this.id = customerEntity.getId();
        this.firstName = customerEntity.getFirstName();
        this.lastName = customerEntity.getLastName();
        this.afm = customerEntity.getAfm();
        this.phoneNumber = customerEntity.getPhoneNumber();
        this.email = customerEntity.getEmail();
        this.products = customerEntity.getProducts().stream().map(x -> new ProductDto(x)).collect(Collectors.toList());
    }

    public void addProduct(ProductDto product) {
        if (Objects.isNull(this.products)) {
            this.products = new ArrayList<>();
        }
        this.products.add(product);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }
}
